
(defparameter *scheme-procs*
  '(+ - * / = < > <= >= cons car cdr not append list read member
    (null? null) (eq? eq) (equal? equal) (eqv? eql)
    (write prin1) (display princ) (newline terpri)))

(defun set-global-var! (var val)
  (setf (get var 'global-val) val))

(defun get-global-val (var)
  (let* ((default "unbound")
         (val (get var 'global-val default)))
    (if (eq val default)
        (error "Unbound Scheme variable ~a" var)
        val)))

(defun set-var! (var val env)
  "Set a variable to a value, in the given or global environment."
  (if (assoc var env)
      (setf (second (assoc var env)) val)
      (set-global-var! var val)))

(defun get-var (var env)
  "Get the value of a variable, from the given or global environment."
  (if (assoc var env)
      (second (assoc var env))
      (get-global-val var)))

(defun extend-env (vars vals env)
  "Add some variables and values to an environment"
  (nconc (mapcar #'list vars vals) env))

(defun init-scheme-proc (f)
  "Define a Scheme procedure as a corresponding CL function."
  (if (listp f)
      (set-global-var! (first f) (symbol-function (second f)))
      (set-global-var! f (symbol-function f))))

(defun init-scheme-interp ()
  "Initialize the scheme interpreter with some global variables."
  ;; define Scheme procedures as CL functions
  (mapc #'init-scheme-proc *scheme-procs*)
  ;; define the boolean 'constants'
  ;; this will not prohibit saying '(set! t nil)'
  (set-global-var! t t)
  (set-global-var! nil nil))

(defun length=1 (x)
  "Is x a list of length 1?"
  (and (consp x) (null (cdr x))))

(defun maybe-add (op exps &optional if-nil)
  "For example, (maybe-add 'and exps t) returns t, if exps is nil,
exps, if there is only one, and (and exp1 exp2 ...) if there are several exps."
  (cond ((null exps) if-nil)
        ((length=1 exps) (first exps))
        (t (cons op exps))))

(defun last1 (list)
  "Return the last element (not the last cons cell) of list."
  (first (last list)))

(defun scheme-macro (symbol)
  (and (symbolp symbol) (get symbol 'scheme-macro)))

(defmacro def-scheme-macro (name parmlist &body body)
  "Define a Scheme macro."
  `(setf (get ',name 'scheme-macro)
         #'(lambda ,parmlist .,body)))

(defun scheme-macro-expand (x)
  "Macro-expand this Scheme expression."
  (if (and (listp x) (scheme-macro (first x)))
      (scheme-macro-expand
       (apply (scheme-macro (first x)) (rest x)))
      x))

(defstruct (proc (:print-function print-proc))
  "Represent a Scheme procedure"
  code (env nil) (name nil) (parms nil))

(defun print-proc (proc &optional (stream *standard-output*) depth)
  (declare (ignore depth))
  (format stream "{~a}" (or (proc-name proc) '??)))

(defun interp (x &optional env)
  "Interpret (evaluate) the expression x in the environment env."
  (prog ()
   :INTERP
     (return
      (cond ((symbolp x) (get-var x env))
            ((atom x) x)
            ((scheme-macro (first x))
             (setf x (scheme-macro-expand x)) (go :INTERP))
            ((case (first x)
               (QUOTE (second x))
               (BEGIN (pop x) ; pop off the BEGIN to get to the args
                ;; now interpret all but the last expression
                (loop while (rest x) do (interp (pop x) env))
                ;; finally, rename the last expression as x
                (setf x (first x))
                (go :INTERP))
               (SET! (set-var! (second x) (interp (third x) env) env))
               (IF (setf x (if (interp (second x) env)
                       (third x)
                       (fourth x)))
                   ;; that is, rename the right expression as x
                   (go :INTERP))
               (LAMBDA (make-proc :env env :parms (second x)
                        :code (maybe-add 'begin (cddr x))))
               (t ;; a procedure application
                (let ((proc (interp (first x) env))
                      (args (mapcar #'(lambda (v) (interp v env))
                                    (rest x))))
                  (if (proc-p proc)
                      ;; execute procedure with rename+goto
                      (progn
                        (setf x (proc-code proc))
                        (setf env (extend-env (proc-parms proc) args
                                              (proc-env proc)))
                        (go :INTERP))
                      ;; else apply primitive procedure
                      (apply proc args))))))))))

(defun scheme ()
  "A Scheme read-eval-print loopusing interp."
  (init-scheme-interp)
  (loop (format t "~&scheme> ")
        (print (interp (read) nil))))

(def-scheme-macro let (bindings &rest body)
  `((lambda ,(mapcar #'first bindings) . ,body)
    .,(mapcar #'second bindings)))

(assert (equal (scheme-macro-expand '(let ((x 1) (y 2)) (+ x y)))
               '((LAMBDA (X Y) (+ X Y)) 1 2)))

(def-scheme-macro let* (bindings &rest body)
  (if (null bindings)
      `(begin .,body)
      `(let (,(first bindings))
         (let* ,(rest bindings) . ,body))))

(assert (equal (scheme-macro-expand '(let* ((x 1) (y (* 2 x)) (+ x y))))
               '((LAMBDA (X) (LET* ((Y (* 2 X)) (+ X Y)))) 1)))

(def-scheme-macro and (&rest args)
  (cond ((null args) 'T)
        ((length=1 args) (first args))
        (t `(if ,(first args)
                (and . ,(rest args))))))

(assert (equal (scheme-macro-expand '(and p q))
               '(IF P (AND Q))))

(assert (equal (scheme-macro-expand '(and q))
               'Q))

(def-scheme-macro or (&rest args)
  (cond ((null args) 'NIL)
        ((length=1 args) (first args))
        (t (let ((var (gensym)))
             `(let ((,var ,(first args)))
                (if ,var ,var (or . ,(rest args))))))))

(defun starts-with (list x)
  "Is this a list whose first element is x?"
  (and (consp list) (eql (first list) x)))

(def-scheme-macro cond (&rest clauses)
  (cond ((null clauses) 'NIL)
        ((length=1 (first clauses))
         `(or ,(first clauses) (cond .,(rest clauses))))
        ((starts-with (first clauses) 'else)
         `(begin .,(rest (first clauses))))
        (t `(if ,(first (first clauses))
                (begin .,(rest (first clauses)))
                (cond .,(rest clauses))))))

(def-scheme-macro case (key &rest clauses)
  (let ((key-val (gensym "KEY")))
    `(let ((,key-val,key))
       (cond ,@(mapcar #'(lambda (clause)
                           (if (starts-with clause 'else)
                               clause
                               `((member ,key-val ',(first clause))
                                 .,(rest clause))))
                       clauses)))))

(def-scheme-macro define (name &rest body)
  (if (atom name)
      `(begin (set! ,name . ,body) ',name)
      `(define ,(first name)
           (lambda ,(rest name) . ,body))))

(def-scheme-macro delay (computation)
  `(lambda () ,computation))

(def-scheme-macro letrec (bindings &rest body)
  `(let ,(mapcar #'(lambda (v) (list (first v ) nil)) bindings)

     .,body))





